﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CodingTestApplication.Interfaces
{
    /// <summary>
    /// defines employee classes
    /// </summary>
    public interface IEmployee
    {
        public float VacationDays { get; set; }
        public void Work(int daysWorked);
        public void TakeVacation(float daysTaken);
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public EmployeeType EmployeeType { get; set; }
        public int NumberOfVacationDays { get; set; }
    }
    /// <summary>
    /// defines employee types in the company
    /// </summary>
    public enum EmployeeType
    {
        Manager,
        Contractor,
        FullTimer
    }
}
