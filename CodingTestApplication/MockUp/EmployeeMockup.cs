﻿using CodingTestApplication.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CodingTestApplication.MockUp
{
    public class EmployeeMockup
    {
        public class EmployeeMockModel
        {
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public EmployeeType employeeType { get; set; }
        }
        public List<EmployeeMockModel> GetAllEmployeeInfo()
        {
            List<EmployeeMockModel> employeeMockup = new List<EmployeeMockModel>();
            employeeMockup.Add(new EmployeeMockModel() { FirstName = "Joe", LastName = "Sam", employeeType = EmployeeType.Manager });
            employeeMockup.Add(new EmployeeMockModel() { FirstName = "John", LastName = "Smith", employeeType = EmployeeType.Manager });
            employeeMockup.Add(new EmployeeMockModel() { FirstName = "Mara", LastName = "Green", employeeType = EmployeeType.Manager });
            employeeMockup.Add(new EmployeeMockModel() { FirstName = "Tima", LastName = "Smith", employeeType = EmployeeType.Manager });
            employeeMockup.Add(new EmployeeMockModel() { FirstName = "Todd", LastName = "Garrett", employeeType = EmployeeType.Manager });
            employeeMockup.Add(new EmployeeMockModel() { FirstName = "Will", LastName = "Chad", employeeType = EmployeeType.Manager });
            employeeMockup.Add(new EmployeeMockModel() { FirstName = "Brian", LastName = "Bryan", employeeType = EmployeeType.Manager });
            employeeMockup.Add(new EmployeeMockModel() { FirstName = "Ryan", LastName = "Ryan", employeeType = EmployeeType.Manager });
            employeeMockup.Add(new EmployeeMockModel() { FirstName = "Mary", LastName = "Richard", employeeType = EmployeeType.Manager });
            employeeMockup.Add(new EmployeeMockModel() { FirstName = "Jennifer", LastName = "Michael", employeeType = EmployeeType.Manager });
            employeeMockup.Add(new EmployeeMockModel() { FirstName = "Susan", LastName = "Richard", employeeType = EmployeeType.FullTimer });
            employeeMockup.Add(new EmployeeMockModel() { FirstName = "Nancy", LastName = "Daniel", employeeType = EmployeeType.FullTimer });
            employeeMockup.Add(new EmployeeMockModel() { FirstName = "Matthew", LastName = "Daniel", employeeType = EmployeeType.FullTimer });
            employeeMockup.Add(new EmployeeMockModel() { FirstName = "Anthony", LastName = "Mark", employeeType = EmployeeType.FullTimer });
            employeeMockup.Add(new EmployeeMockModel() { FirstName = "Ashley", LastName = "Matthew", employeeType = EmployeeType.FullTimer });
            employeeMockup.Add(new EmployeeMockModel() { FirstName = "Mark", LastName = "Patrick", employeeType = EmployeeType.FullTimer });
            employeeMockup.Add(new EmployeeMockModel() { FirstName = "Paul", LastName = "Walter", employeeType = EmployeeType.FullTimer });
            employeeMockup.Add(new EmployeeMockModel() { FirstName = "Steven", LastName = "Alexander", employeeType = EmployeeType.FullTimer });
            employeeMockup.Add(new EmployeeMockModel() { FirstName = "Andrew", LastName = "Austin", employeeType = EmployeeType.FullTimer });
            employeeMockup.Add(new EmployeeMockModel() { FirstName = "Kenneth", LastName = "Walter", employeeType = EmployeeType.FullTimer });
            employeeMockup.Add(new EmployeeMockModel() { FirstName = "Joshua", LastName = "Jack", employeeType = EmployeeType.Contractor });
            employeeMockup.Add(new EmployeeMockModel() { FirstName = "George", LastName = "Patrick", employeeType = EmployeeType.Contractor });
            employeeMockup.Add(new EmployeeMockModel() { FirstName = "Edward", LastName = "Raymond", employeeType = EmployeeType.Contractor });
            employeeMockup.Add(new EmployeeMockModel() { FirstName = "Ryan", LastName = "Benjamin", employeeType = EmployeeType.Contractor });
            employeeMockup.Add(new EmployeeMockModel() { FirstName = "Jason", LastName = "Timothy", employeeType = EmployeeType.Contractor });
            employeeMockup.Add(new EmployeeMockModel() { FirstName = "Kathleen", LastName = "Scott", employeeType = EmployeeType.Contractor });
            employeeMockup.Add(new EmployeeMockModel() { FirstName = "Cynthia", LastName = "Nicholas", employeeType = EmployeeType.Contractor });
            employeeMockup.Add(new EmployeeMockModel() { FirstName = "Sharon", LastName = "Timothy", employeeType = EmployeeType.Contractor });
            employeeMockup.Add(new EmployeeMockModel() { FirstName = "Laura", LastName = "Jason", employeeType = EmployeeType.Contractor });
            employeeMockup.Add(new EmployeeMockModel() { FirstName = "Ali", LastName = "Sanad", employeeType = EmployeeType.Contractor });

            return employeeMockup;
        }
    }
}
