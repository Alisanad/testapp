﻿using CodingTestApplication.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CodingTestApplication.Controllers
{
    /// <summary>
    /// enherits from IEmployee interface 
    /// and return employee object in accordance
    /// to the contstructor params
    /// </summary>
    public class Employee : IEmployee
    {
        private readonly ILogger<HomeController> _logger;
        public float VacationDays { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public EmployeeType EmployeeType { get; set; }
        public int NumberOfVacationDays { get; set; } //this can be a delegate
        public Employee(ILogger<HomeController> logger,
            int numOfDays,
            EmployeeType employeeType,
            string _firstName,
            string _lastName
            )
        {
            FirstName = _firstName;
            LastName = _lastName;
            EmployeeType = employeeType;
            NumberOfVacationDays = numOfDays;
            _logger = logger;

        }
        public void Work(int daysWorked)
        {
            if (daysWorked > 260)
            {
                Console.WriteLine("Employees can't work more than 260 days");
                return;
            }
            VacationDays += NumberOfVacationDays;
        }
        public void TakeVacation(float daysTaken)
        {
            if (VacationDays - daysTaken < 0)
            {
                Console.Write("Insufficient vacation days left");
                return;
            }
            VacationDays -= daysTaken;
        }
    }
}
