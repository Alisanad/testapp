﻿using CodingTestApplication.Interfaces;
using CodingTestApplication.MockUp;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CodingTestApplication.Controllers
{
    /// <summary>
    /// Employee builder with static methods for each employee type
    /// The class will handle logger instances
    /// number of vacation days
    /// and employee type
    /// </summary>
    public class EmployeeFactory
    {
        public static IEmployee CrateEmployee(string firstName, string lastName, EmployeeType employeeType, ILogger<HomeController> logger)
        {
            switch (employeeType)
            {
                case EmployeeType.Contractor:
                    return new Employee(logger, 10, EmployeeType.Contractor, firstName, lastName);

                case EmployeeType.FullTimer:
                    return new Employee(logger, 15, EmployeeType.FullTimer, firstName, lastName);

                case EmployeeType.Manager:
                    return new Employee(logger, 30, EmployeeType.Manager, firstName, lastName);
                default:
                    return null;
            }
        }
    }
}
